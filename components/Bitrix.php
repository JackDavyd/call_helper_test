<?php


namespace app\components;

use yii2mod\user\models\SignupForm;
use yii2mod\user\models\UserModel;
use yii\httpclient\Client;

class Bitrix extends \yii\base\Component
{
    public $DOMAIN;
    private $client = null;

    public function auth($post, $DOMAIN)
    {
        $this->client = new Client(['baseUrl' => 'https://' . $DOMAIN . '/rest']);
        $bitrixResponse = $this->client->get('user.current.json', [
            'auth' => $post['AUTH_ID']
        ])->send();
        if ($bitrixResponse->isOk) {
            $userData = $bitrixResponse->data;
        }
        if (empty($userData)) {
            return false;
        }
        $bitrixUser = !empty($userData['result']) ? $userData['result'] : [];
        if (!$bitrixUser) {
            return false;
        }
        $model = new SignupForm();
        $model->load(['username'=>$bitrixUser['NAME'], 'email'=>$bitrixUser['EMAIL'], 'password'=>$post['AUTH_ID']], '');
        $model->signup();
        $localUser = UserModel::findByEmail($bitrixUser['EMAIL']);
        \Yii::$app->user->login($localUser, 3600 * 24 * 30);
        return $localUser;
    }

    public function getLeads($post, $DOMAIN){
        if(!$this->client){
            $this->client = new Client(['baseUrl' => 'https://' . $DOMAIN . '/rest']);
        }
        $bitrixResponseDeals = $this->client->get('crm.deal.list', [
            'auth' => $post['AUTH_ID'],
            //Сортировка по названию
            'order'=>['BEGINDATE'=>'DESC'],
            //Нужные поля
            'select'=>['TITLE', 'STAGE_ID'],
            //Первые
            'start'=>0,
            //По идее 5 элементов, но бесполезно, всегда 50
            'limit'=>5,
        ])->send();
        if ($bitrixResponseDeals->isOk) {
            $dealsData = $bitrixResponseDeals->data;
        }
        if (empty($dealsData)){
            return [];
        }
        $leads = !empty($dealsData['result']) ? $dealsData['result'] : [];
        $leads = array_slice($leads, 0,5);
        $ac = array_column($leads, 'TITLE');
        array_multisort ($ac, SORT_ASC, $leads);
        //Обрезали до 5 элементов ибо limit при запросе не работает
        return $leads;
    }

    public function saveLeads($leads){
        $data = [];
        foreach($leads as $value){
            $data[] = [$value['TITLE'],$value['STAGE_ID'], \Yii::$app->user->identity->getId()];
        }
        $transaction = \Yii::$app->db->beginTransaction();
        try {

            \Yii::$app
                ->db
                ->createCommand()
                ->delete('leads', ['user_id' => \Yii::$app->user->identity->getId()])
                ->execute();
            \Yii::$app->db
                ->createCommand()
                ->batchInsert('leads', ['name','state', 'user_id'],$data)
                ->execute();

            $transaction->commit();

        } catch(\Exception $e) {
            $transaction->rollBack();
            throw $e;
        }
    }

    public function getLocalLeads(){

    }
}