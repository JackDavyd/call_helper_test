<?php
/* @var $this yii\web\View */

use yii\helpers\Html;
use yii\grid\GridView;
?>
<p>

    <?= Html::a('Обновить сделки из Битрикс',
        ['bitrix/update'],
        [
            'class' => 'btn btn-success',
            'data' => [
                'method' => 'post',
                'params' => ['post' => json_encode($post), 'domain' => $domain], // <- extra level
            ],
        ]
    ); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
    ]); ?>
</p>
