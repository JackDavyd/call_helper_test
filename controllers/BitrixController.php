<?php

namespace app\controllers;

use Yii;
use app\models\Leads;
use app\models\LeadsSearch;
use yii2mod\user\models\SignupForm;
use yii2mod\user\models\UserModel;
use yii\helpers\Url;
class BitrixController extends \yii\web\Controller
{
    private $post = null;

    /**
     * Отключили Csrf проверку Битрикс дает другой ключ
     * @param \yii\base\Action $action
     * @return bool
     * @throws \yii\web\BadRequestHttpException
     */
    public function beforeAction($action)
    {
        if ($this->action->id == 'index' || $this->action->id == 'update') {
            $this->enableCsrfValidation = false;
        }
        return parent::beforeAction($action);
    }

    /**
     * Первая страница
     * @param $DOMAIN
     * @param string $AUTH_ID
     * @return string
     */
    public function actionIndex($DOMAIN, $AUTH_ID='')
    {
        //Получаем $_POST
        $post = Yii::$app->request->post();
        //Если $_POST есть авторизуем пользователя и выводим лиды из БД, если нет просто выводим лиды из БД
        if($post) {
            $user = Yii::$app->bitrix->auth($post, $DOMAIN);
            $searchModel = new LeadsSearch();
            $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        }else{
            $searchModel = new LeadsSearch();
            $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
            $post['AUTH_ID'] = $AUTH_ID;
            return $this->render('index',['post'=>$post, 'domain'=>$DOMAIN, 'searchModel' => $searchModel,
                'dataProvider' => $dataProvider,]);
        }
        return $this->render('index',['post'=>$post, 'domain'=>$DOMAIN, 'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,]);
    }

    /**
     * Обработчик кнопки обновления лидов в БД
     * @return \yii\web\Response
     */
    public function actionUpdate(){
        $post = Yii::$app->request->post();
        $userPost = json_decode($post['post'],1);
        $leads = Yii::$app->bitrix->getLeads($userPost, $post['domain']);
        Yii::$app->bitrix->saveLeads($leads);
        return $this->redirect(['index', 'DOMAIN'=>$post['domain'], 'AUTH_ID'=>$userPost['AUTH_ID']]);
    }

}
